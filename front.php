<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Le blog - Acceuil</title>
    <link rel="icon" type="image/x-icon" href="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="header">
        <img id="logo" src="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png" alt="">
        <a id="title" href="">Le blog du ciel</a>
        <div id="menus">
        <a id="menu1" class="menu" href="">Inscription</a>
        <a id="menu2" class="menu" href="">Connexion</a>
        </div>
    </div>
    <div id="box1">Les posts du jour</div>
<?php
require_once("pdo.php");

$sql = "SELECT  `contenu` FROM post";
//echo $request_query ;
try{
    $pdo = new PDO($connex, $user, $psw);
    $stmt = $pdo->query($sql);
    if($stmt === false){
     die("Erreur");
    }
   }catch (PDOException $e){
     echo $e->getMessage();
   }
?>
<?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
    <div class="box"><?php echo htmlspecialchars($row['contenu']); ?>
    <button>commenter</button><button>like</button>
    </div>
        
    <?php endwhile; ?>
</body>
</html>