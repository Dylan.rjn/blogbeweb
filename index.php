

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Le blog - Inscription</title>
    <link rel="icon" type="image/x-icon" href="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png">
    <link rel="stylesheet" href="../css/insc.css">
</head>
<body>
<div id="header">
        <img id="logo" src="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png" alt="">
        <a id="title" href="">Le blog du ciel</a>
        <div id="menus">
        <a id="menu1" class="menu" href="">Inscription</a>
        <a id="menu2" class="menu" href="">Connexion</a>
        </div>
    </div>
    <div id="divInscrit">
        <img id="logo1" src="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png" alt="">
    <form method='post' action='inscri_create.php'>
        <input type="text" name="nom" placeholder="nom :">
        <input type="text" name="prenom" placeholder="prenom :">
        <input type="text" name="pseudo" placeholder="pseudo :">
        <input type="text" name="email" placeholder="email :">
        <input type="number" name="telephone" placeholder="telephone :">
        <input type="password" name="mdp" placeholder="password :">
        <button>Inscrit</button>
    </form>
</div>
</body>

<script>

<?php
require_once("pdo.php");
    if($_GET){
        echo $_GET['msg'];
    }
$read_query = "SELECT * FROM `user`";

try {
    $sth = $dbh->query($read_query)->fetchAll(PDO::FETCH_ASSOC) ;  
}
catch (PDOException $e){
    echo "read failed : ".$e->getMessage();
}
?>

</script>
</html>