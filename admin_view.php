<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Le blog - Admin</title>
    <link rel="icon" type="image/x-icon" href="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
<div id="header">
        <img id="logo" src="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png" alt="">
        <a id="title" href="">Le blog du ciel</a>
        <div id="menus">
        <a id="menu1" class="menu" href="">Inscription</a>
        <a id="menu2" class="menu" href="">Connexion</a>
        </div>
    </div>
    <div id="box1">posts a moderer</div>
    <?php
    require('admin_control.php');
    ?>
</body>
</html>