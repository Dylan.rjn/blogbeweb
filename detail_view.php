<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Le blog - Detail</title>
    <link rel="icon" type="image/x-icon" href="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
<div id="header">
        <img id="logo" src="https://i.skyrock.net/5738/33265738/pics/photo_33265738_20.png" alt="">
        <a id="title" href="">Le blog du ciel</a>
        <div id="menus">
        <a id="menu1" class="menu" href="">Inscription</a>
        <a id="menu2" class="menu" href="">Connexion</a>
        </div>
    </div>
    <div id="box1">Le post sur lequel t'as cliqué</div>
    <div id="post" class="box">
        <div id="postcont">
        <?php
        require_once("pdo.php");
        $sql = "SELECT contenu FROM `post`";
        $result = $dbh->query($sql);
        
        if ($result->num_rows > 0) {
            echo "<table>";
            while($row = $result->fetch_assoc()) {
              echo "<tr><td>".$row["id"]."</td><td>".$row["contenu"]."</td></tr>";
            }
            echo "</table>";
          } else {
            echo "0 results";
          }
        ?>
        </div>
    </div>
    <div class="box">elcommentos</div>
    <div class="box">
        <form method='post' action='com_create.php'>
            <textarea id="com_cont" class="text" cols="100" rows ="20" name="com_cont" placeholder=""></textarea>
            <button>poster</button>
        </form>
    </div>
</body>
</html>